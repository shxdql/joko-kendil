using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private float moveSpeed;

    private int health = 2;

    private void FixedUpdate()
    {
        if (!IsFacingLeft())
            rb.velocity = new Vector2(moveSpeed, 0f);
        else
            rb.velocity = new Vector2(-moveSpeed, 0f);
    }

    private bool IsFacingLeft()
    {
        return transform.localScale.x > Mathf.Epsilon;
    }

    private void Flip()
    {
        transform.localScale = new Vector2(-transform.localScale.x, transform.localScale.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "PosPatrol")
            Flip();

        if (collision.gameObject.tag == "Player")
        {
            if (health > 1)
                health--;
            else
                Destroy(gameObject);
        }
    }
}
