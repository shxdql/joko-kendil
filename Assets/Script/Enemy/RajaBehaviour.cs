using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RajaBehaviour : MonoBehaviour
{
    [SerializeField] private GameObject bullet;
    [SerializeField] private Transform bulletPos;

    private float timer = 0f;
    private int health = 2;

    private void Start()
    {
        Shoot();
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if (timer > 2f)
        {
            timer = 0f;
            Shoot();
        }
    }

    private void Shoot()
    {
        Instantiate(bullet, bulletPos.position, Quaternion.identity);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if (health > 1)
                health--;
            else
                Destroy(gameObject);
        }
    }
}
