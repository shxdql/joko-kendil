using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level1 : MonoBehaviour
{
    public Transform finishPos;
    public Transform playerPos;

    private void Update()
    {
        if (Vector2.Distance(playerPos.position, finishPos.position) < 1f)
            SceneManager.LoadScene("Level2");
    }
}
