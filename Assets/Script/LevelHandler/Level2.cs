using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level2 : MonoBehaviour
{
    [SerializeField] private GameObject enemy;

    private void Update()
    {
        if (enemy == null)
            SceneManager.LoadScene("Level3");
    }
}
