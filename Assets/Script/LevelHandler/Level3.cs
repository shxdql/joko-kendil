using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level3 : MonoBehaviour
{
    [SerializeField] private GameObject panel, enemy, raja;

    private void Update()
    {
        if (enemy == null && raja == null)
            StartCoroutine(Finish());
    }

    IEnumerator Finish()
    {
        panel.SetActive(true);
        yield return new WaitForSecondsRealtime(3f);
        SceneManager.LoadScene("MainMenu");
    }
}
